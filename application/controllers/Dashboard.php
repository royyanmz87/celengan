<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        cek_login();
        // // $this->load->model('Auth_model', 'auth');
        // $this->load->model('Admin_model', 'admin');
    }

    public function index()
    {
        $login = $this->session->userdata('id_user');
        $data['title'] = "Dashboard";
        $data['satuanIn'] = $this->base_model->get_limit_masuk()->result();
        $data['satuanOut'] = $this->base_model->get_limit_keluar()->result();
        $data['cash'] = $this->base_model->getCash()->result();
        $this->template->load('template', 'dashboard/dashboard', $data);
    }

    public function addsaldo()
    {
        $this->db->insert('cash_balance', array(
            'date' => date('Y-m-d H:i:s'),
            'mutation' => $this->input->post('mutation'),
            'amount' => str_replace(',', '', $this->input->post('amount')),
            'description' => $this->input->post('description'),
            'id_user' => userdata('id_user')
        ));

        if ($this->db->affected_rows()) {
            $this->data = array(
                'status' => true,
                'message' => "Data berhasil disimpan"
            );
        } else {
            $this->data = array(
                'status' => false,
                'message' => "Gagal saat menyimpan data!"
            );
        }

        redirect('dashboard');
    }
}
